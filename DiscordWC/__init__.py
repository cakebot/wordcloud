from PIL.Image import Image
from wordcloud import WordCloud


class DiscordWC:
    def __init__(self, channel, bg="#eee"):
        self.channel = channel
        self.color = bg

    async def __str__(self):
        messages = ""
        async for msg in self.channel.history(limit=100):
            messages += msg.content + " "
        return messages

    def generate(self) -> Image:
        wordcloud = WordCloud(
            width = 1000,
            height = 500,
            background_color = self.color
        ).generate(str(self))
        return wordcloud.to_image()
