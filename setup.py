import setuptools

setuptools.setup(
    name='DiscordWC',
    version='1.3',
    description='Wrapper for creating word clouds from Discord message history',
    packages=setuptools.find_packages(),
    author='Cakebot Team',
    author_email='me@rdil.rocks',
    url='https://github.com/cakebotpro/worldcloud',
    install_requires=[
        "wordcloud==1.6.0",
        "pillow==7.0.0"
    ],
    include_package_data=True
)
